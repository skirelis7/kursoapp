﻿namespace coursesApp.Models
{
	public class Lecturer : Person
	{
		public Lecturer(
			int id, 
			string name, 
			string surname, 
			string email, 
			string employeeId, 
			bool isRetired) 
			: base(
				id, 
				name, 
				surname, 
				email)
		{
			EmployeeId = employeeId;
			IsRetired = isRetired;
		}

		public string EmployeeId { get; }

		public bool IsRetired { get; }

		public override string ToString()
		{
			return $"{base.ToString()};{EmployeeId};{IsRetired}";
		}
	}
}
