﻿namespace coursesApp.Models
{
	public class Person : ModelBase
	{
		public Person(
			int id, 
			string name, 
			string surname,
			string email) 
			: base(id)
		{
			Name = name;
			Surname = surname;
			Email = email;
		}

		public string Name { get; }

		public string Surname { get; }

		public string Email { get; set; }

		public override string ToString()
		{
			return $"{base.ToString()};{Name};{Surname};{Email}";
		}
	}
}
