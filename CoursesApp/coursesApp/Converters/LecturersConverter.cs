﻿using coursesApp.Models;

namespace coursesApp.Converters
{
	public class LecturersConverter
	{
		public List<Lecturer> Convert(List<string> lines)
		{
			if (lines.Count == 0)
			{
				return new List<Lecturer>();
			}

			List<Lecturer> lecturers = new List<Lecturer>();

			foreach (string line in lines)
			{
				lecturers.Add(CreateLecturer(line));
			}

			return lecturers;
		}

		private Lecturer CreateLecturer(string line)
		{
			string[] splitLine = line.Split(';');

			return new Lecturer(
				int.Parse(splitLine[0]),
				splitLine[1],
				splitLine[2],
				splitLine[3],
				splitLine[4],
				bool.Parse(splitLine[5])
			);
		}
	}
}
