﻿using coursesApp.Models;

namespace coursesApp.Converters
{
	public class StudentsConverter
	{
		public List<Student> Convert(List<string> lines)
		{
			if (lines.Count == 0)
			{
				return new List<Student>();
			}

			List<Student> students = new List<Student>();

			foreach (string line in lines)
			{
				students.Add(CreateStudent(line));
			}

			return students;
		}

		private Student CreateStudent(string line)
		{
			string[] splitLine = line.Split(';');

			return new Student(
				int.Parse(splitLine[0]),
				splitLine[1],
				splitLine[2],
				splitLine[3],
				ResolveCourse(splitLine[4])
			);
		}

		private Course ResolveCourse(string course)
		{
			switch (course.ToLower())
			{
				case "first":
				case "1":
					return Course.First;
				
				case "second":
				case "2":
					return Course.Second;
				
				case "third":
					case "3":
					return Course.Third;

				case "fourth":
				case "4":
					return Course.Fourth;

				default: throw new NotSupportedException();
			}
		}
	}
}
