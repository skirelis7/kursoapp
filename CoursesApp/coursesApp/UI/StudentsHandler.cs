﻿using coursesApp.Converters;
using coursesApp.Models;
using coursesApp.Repositories;
using Newtonsoft.Json;

namespace coursesApp.UI
{
	public class StudentsHandler
	{
		private readonly FilesRepository repository;
		private readonly StudentsConverter studentsConverter;

		public StudentsHandler(
			FilesRepository repository,
			StudentsConverter studentsConverter)
		{
			this.repository = repository;
			this.studentsConverter = studentsConverter;
		}

		public void Handle()
		{
			var continueKey = "t";
			do
			{
				Console.WriteLine("Patekote į studentų aplinką");
				Console.WriteLine("Ką norite daryti");
				Console.WriteLine("1 - eksportuoti studentus į JSON failą");
				Console.WriteLine("2 - išspausdinti studentus ekrane");
				Console.WriteLine("3 - sukurti studentą");
				Console.WriteLine("4 - pakeisti studentą");
				Console.WriteLine("5 - ištrinti studentą");

				// viduriai
				Console.WriteLine("Įveskite ką renkatės");
				switch (Console.ReadLine())
				{
					case "1":
						ExportToJsonFile();
						break;
					case "2":
						PrintStudentsToScreen();
						break;
					case "3":
						CreateStudent();
						break;
					case "4":
						ChangeStudent();
						break;
					case "5":
						DeleteStudent();
						break;
					default:
						Console.WriteLine("Nėra tokio pasirinkimo");
						break;
				}

				Console.WriteLine("Ar tęsiame su studentais? (t/n)");
				continueKey = Console.ReadLine();
			} while (continueKey == "t");
		}

		private void ExportToJsonFile()
		{
			List<string> lines = repository.Get();

			List<Student> students = studentsConverter.Convert(lines);

			// from objects to json
			var jsString = JsonConvert.SerializeObject(students);

			ExportToJson(jsString);
			Console.WriteLine("Failas students.json exportuotas");
		}

		private void PrintStudentsToScreen()
		{
			List<string> lines = repository.Get();

			List<Student> students = studentsConverter.Convert(lines);

			Console.WriteLine("Studentų sąrašas:");
			Console.WriteLine($"Id;Vardas;Pavardė;Email;Course");
			foreach (var student in students)
			{
				Console.WriteLine(student);
			}
		}

		private void CreateStudent()
		{
			Console.WriteLine("Suveskite studento duomenis");

			Console.WriteLine("Iveskite id");
			int id = int.Parse(Console.ReadLine());

			Console.WriteLine("Iveskite varda");
			var name = Console.ReadLine();

			Console.WriteLine("Iveskite pavarde");
			var surname = Console.ReadLine();

			Console.WriteLine("Iveskite kursa");
			string course = Console.ReadLine();

			Student student = new Student(
				id,
				name,
				surname,
				$"{name}{surname}@vilniuscoding.lt",
				ResolveCourse(course)
			);

			repository.Save(new List<string> { student.ToString() });

			Console.WriteLine("Studentas sukurtas");

		}

		private void ChangeStudent()
		{
			Console.WriteLine("Suveskite studento duomenis, kuriuos keisite");

			Console.WriteLine("Kokį studenta keisite? Pasirinkite id iš sąrašo.");
			PrintStudentsToScreen();

			Console.WriteLine("Iveskite id");
			int id = int.Parse(Console.ReadLine());

			Console.WriteLine("Iveskite varda");
			var name = Console.ReadLine();

			Console.WriteLine("Iveskite pavarde");
			var surname = Console.ReadLine();

			Console.WriteLine("Iveskite kursa");
			string course = Console.ReadLine();

			List<string> lines = repository.Get();

			List<Student> students = studentsConverter.Convert(lines);

			Student newStudent = new Student(
				id,
				name,
				surname,
				$"{name}{surname}@vilniuscoding.lt",
				ResolveCourse(course)
			);

			ReplaceStudent(students, id, newStudent);

			repository.Rewrite(
				students
					.Select(x => x.ToString())
					.ToList()
			);

			Console.WriteLine("Studentas pakeistas");
		}

		private void DeleteStudent()
		{
			Console.WriteLine("Kokį studenta trinsite? Pasirinkite id iš sąrašo.");
			PrintStudentsToScreen();

			Console.WriteLine("Įveskite id");
			var id = int.Parse(Console.ReadLine());

			Console.WriteLine("Ar tikrai to norite? t/n");

			if (Console.ReadLine() == "t")
			{
				List<string> lines = repository.Get();

				List<Student> students = studentsConverter.Convert(lines);

				students.RemoveAll(student => student.Id == id);

				repository.Rewrite(
					students
						.Select(x => x.ToString())
						.ToList()
				);

				Console.WriteLine($"Studentas ištrintas");
			}
			else
			{
				Console.WriteLine("Studentas nebuvo ištrintas");
			}
		}

		private void ExportToJson(string json)
		{
			using (var streamWriter = new StreamWriter("students.json"))
			{
				streamWriter.WriteLine(json);
			}
		}

		private void ReplaceStudent(
			List<Student> students, 
			int id, 
			Student newStudent)
		{
			for (int i = 0; i < students.Count; i++)
			{
				if (students[i].Id == id)
				{
					students[i] = newStudent;
					break;
				}
			}
		}

		private Course ResolveCourse(string course)
		{
			switch (course.ToLower())
			{
				case "first":
				case "1":
					return Course.First;

				case "second":
				case "2":
					return Course.Second;

				case "third":
				case "3":
					return Course.Third;

				case "fourth":
				case "4":
					return Course.Fourth;

				default: throw new NotSupportedException();
			}
		}
	}
}
