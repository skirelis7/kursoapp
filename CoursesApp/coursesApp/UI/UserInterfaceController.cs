﻿using coursesApp.Converters;
using coursesApp.Repositories;

namespace coursesApp.UI
{
	public class UserInterfaceController
	{
		public void Run()
		{
			string continueKey = "t";

			do
			{
				Console.WriteLine("Sveiki! Ką norite veikti?");
				Console.WriteLine("1 - kurso valdymas");
				Console.WriteLine("2 - lektorių valdymas");
				Console.WriteLine("3 - studentų valdymas");
				Console.WriteLine("4 - vykstančių kursų valdymas");

				// visi viduriai čia

				try
				{
					Console.WriteLine("Įveskite ką renkatės");
					switch (Console.ReadLine())
					{
						case "1":
							new CoursesHandler().Handle();
							break;
						case "2":
							new LecturersHandler(
								new FilesRepository("lecturers.csv"),
								new LecturersConverter()
							).Handle();
							break;
						case "3":
							new StudentsHandler(
								new FilesRepository("students.csv"),
								new StudentsConverter()
							).Handle();
							break;
						case "4":
							throw new NotImplementedException();
							break;
						default:
							Console.WriteLine("Nėra tokio pasirinkimo");
							break;
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine("Įvyko bėda. Klauskite administratoriaus.");
					LogToFile(ex.ToString());
				}

				Console.WriteLine("Esate pagrindineje dalyje. Ar bandom iš naujo? (t/n)");
				continueKey = Console.ReadLine();
			} while (continueKey == "t");
		}

		private void LogToFile(string text)
		{
			using var streamWriter = new StreamWriter("logs.txt", true);
			
			streamWriter.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")} - {text}");
		}
	}
}
