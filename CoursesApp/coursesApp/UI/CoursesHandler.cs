﻿namespace coursesApp.UI
{
	public class CoursesHandler
	{
		public void Handle()
		{
			var continueKey = "t";
			do
			{
				Console.WriteLine("Patekote į kursų aplinką");
				Console.WriteLine("Ką norite daryti");
				Console.WriteLine("1 - eksportuoti kursus į JSON failą");
				Console.WriteLine("2 - išspausdinti kursus ekrane");
				Console.WriteLine("3 - sukurti kursą");
				Console.WriteLine("4 - pakeisti kursą");
				Console.WriteLine("5 - ištrinti kursą");

				// viduriai
				Console.WriteLine("Įveskite ką renkatės");
				switch (Console.ReadLine())
				{
					case "1":
						ExportToJsonFile();
						break;
					case "2":
						PrintCoursesToScreen();
						break;
					case "3":
						CreateCourse();
						break;
					case "4":
						ChangeCourse();
						break;
					case "5":
						DeleteCourse();
						break;
					default:
						Console.WriteLine("Nėra tokio pasirinkimo");
						break;
				}

				Console.WriteLine("Ar tęsiame su kursais? (t/n)");
				continueKey = Console.ReadLine();
			} while (continueKey == "t");
		}

		private void ExportToJsonFile()
		{
			throw new NotImplementedException();
		}

		private void PrintCoursesToScreen()
		{
			throw new NotImplementedException();
		}

		private void CreateCourse()
		{
			throw new NotImplementedException();
		}

		private void ChangeCourse()
		{
			throw new NotImplementedException();
		}

		private void DeleteCourse()
		{
			throw new NotImplementedException();
		}
	}
}
