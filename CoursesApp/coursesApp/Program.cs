﻿using coursesApp.UI;

namespace coursesApp
{
	internal class Program
	{
		static void Main(string[] args)
		{
			new UserInterfaceController().Run();
		}
	}
}