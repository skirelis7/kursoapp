﻿namespace coursesApp.Repositories
{
	public class FilesRepository
	{
		private string _path;

		public FilesRepository(string path)
		{
			_path = path ?? throw new ArgumentNullException(nameof(path));

			if (!File.Exists(path))
			{
				throw new FileNotFoundException();
			}
		}

		public List<string> Get()
		{
			List<string> lines = new List<string>();

			using StreamReader sr = new StreamReader(_path);

			while (!sr.EndOfStream)
			{
				lines.Add(sr.ReadLine());
			}

			return lines;
		}

		public void Save(List<string> lines)
		{
			using StreamWriter sw = new StreamWriter(_path, true);

			foreach (string line in lines)
			{
				sw.WriteLine(line);
			}
		}

		public void Rewrite(List<string> lines)
		{
			using StreamWriter sw = new StreamWriter(_path);

			foreach (string line in lines)
			{
				sw.WriteLine(line);
			}
		}
	}
}
