using coursesApp.Repositories;

namespace coursesApp.Tests.Repositories
{
	[TestClass]
	public class FilesRepositoryTests
	{
		// Test data
		private string _testFilePath = @"C:\Temp\testfile.txt";

		[TestInitialize]
		public void TestInit()
		{
			if (File.Exists(_testFilePath))
			{
				File.Delete(_testFilePath);
			}

			File.WriteAllLines(
				_testFilePath,
				new[] { "Line0", "Line1", "Line2", "Line3" }
			);
		}

		[TestMethod]
		public void ConstructorShouldThrowFileNotFoundException()
		{
			Assert.ThrowsException<FileNotFoundException>(() => new FilesRepository("non_existent_file.txt"));
		}

		[TestMethod]
		public void ConstructorShouldThrowArgumentNullException()
		{
			Assert.ThrowsException<ArgumentNullException>(() => new FilesRepository(null));
		}

		[TestMethod]
		public void GetShouldReadLinesWithoutHeader()
		{
			var repo = new FilesRepository(_testFilePath);

			var lines = repo.Get();

			Assert.AreEqual(4, lines.Count);
		}

		[TestMethod]
		public void GetShouldReadLinesWithHeader()
		{
			var repo = new FilesRepository(_testFilePath);

			var lines = repo.Get();

			Assert.AreEqual(4, lines.Count);
		}

		[TestMethod]
		public void SaveShouldAddLinesToFile()
		{
			var repo = new FilesRepository(_testFilePath);

			var linesToSave = new List<string>
			{
				"NewLine1", "NewLine2", "NewLine3"
			};

			repo.Save(linesToSave);

			var lines = File.ReadAllLines(_testFilePath);

			Assert.AreEqual(7, lines.Length);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void FilesRepository_InvalidPath_ThrowsException()
		{
			new FilesRepository(null);
		}

		[TestMethod]
		[ExpectedException(typeof(FileNotFoundException))]
		public void FilesRepositoryNonExistingFileThrowsException()
		{
			new FilesRepository("non_existing_file.txt");
		}

		[TestMethod]
		public void FilesRepositoryValidPath_DoesNotThrowException()
		{
			var repository = new FilesRepository(_testFilePath);

			Assert.IsNotNull(repository);
		}

		[TestMethod]
		public void RewriteWritesCorrectData()
		{
			var repository = new FilesRepository(_testFilePath);

			List<string> newLines = new List<string> { "New Line 1", "New Line 2" };
			repository.Rewrite(newLines);

			var writtenLines = File.ReadAllLines(_testFilePath).ToList();

			CollectionAssert.AreEqual(newLines, writtenLines);
		}

		[TestCleanup]
		public void TestCleanup()
		{
			if (File.Exists(_testFilePath))
			{
				File.Delete(_testFilePath);
			}
		}
	}
}