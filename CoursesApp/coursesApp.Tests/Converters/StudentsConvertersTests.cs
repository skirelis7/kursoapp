﻿using coursesApp.Converters;
using coursesApp.Models;

namespace coursesApp.Tests.Converters
{
	public class StudentsConvertersTests
	{
		[TestClass]
		public class StudentsConverterTests
		{
			[TestMethod]
			public void ConvertShouldReturnEmptyListWhenInputIsEmpty()
			{
				var converter = new StudentsConverter();
				var result = converter.Convert(new List<string>());

				Assert.IsNotNull(result);
				Assert.AreEqual(0, result.Count);
			}

			[TestMethod]
			public void ConvertShouldReturnCorrectNumberOfStudents()
			{
				var lines = new List<string>
				{
					"1;John;Doe;JohnDoe@vilniuscoding.lt;first",
					"2;Jane;Smith;JaneSmith@vilniuscoding.lt;2",
				};

				var converter = new StudentsConverter();
				var result = converter.Convert(lines);

				Assert.AreEqual(lines.Count, result.Count);
			}

			[TestMethod]
			public void ConvertShouldCreateCorrectStudent()
			{
				var lines = new List<string>
				{
					"1;John;Doe;JohnDoe@vilniuscoding.lt;first"
				};

				var converter = new StudentsConverter();
				var result = converter.Convert(lines);

				var student = result[0];
				Assert.AreEqual(1, student.Id);
				Assert.AreEqual("John", student.Name);
				Assert.AreEqual("Doe", student.Surname);
				Assert.AreEqual("JohnDoe@vilniuscoding.lt", student.Email);
				Assert.AreEqual(Course.First, student.Course);
			}

			[TestMethod]
			[ExpectedException(typeof(NotSupportedException))]
			public void ConvertShouldThrowNotSupportedExceptionForInvalidCourse()
			{
				var lines = new List<string>
				{
					"1;John;Doe;JohnDoe@vilniuscoding.lt;invalidcourse"
				};

				var converter = new StudentsConverter();
				converter.Convert(lines);
			}
		}
	}
}
